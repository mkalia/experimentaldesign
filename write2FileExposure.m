function write2FileExposure(b_firstTime, expName, subNo, subInitials, blocCounter, val_async, tactileFirst, ...
    tactileOdd, visOdd, correctRes, subRes, stimulusDuration)
    
% correctRes, more odd trials: -1 tactile, 0 simultaneous, 1 visual 

if b_firstTime==1
    if nargin >3
        error('When using this function for the first time give only b_firstTime expName and expName. '); 
    end
else
    if nargin < 12
        error('Set all arguments. Should be 12 in number')
    end
end

% expName,subNo should be strings 
    %name of data file to write to
    dataFileName = [expName '_' num2str(subNo) '.csv'];
    
    %prepare column heads for output
    %tactileOdd : num of tactile odd trials
    %visOdd : : num of visual odd trials
    % correctRes : 1 for tactile 2 for visual. More odd trials
    if (b_firstTime==1)
         %print headers to text file
         %open pointer to data file (write results after each trial)
        dataFilePointer = fopen(fullfile(num2str(subNo), dataFileName), 'w');
        colHeaders = {'blocCounter', 'AsynchronyVal', 'tactileFirst', ...
        'tactileOdd','visOdd', 'correctRes', 'subResponse', ...
        'stimulusDuration'};
        fprintf(dataFilePointer,'%s, %s, %s, %s, %s, %s, %s, %s\n', colHeaders{:});
        fclose(dataFilePointer); 
    elseif(b_firstTime==2) % last 
%         dataFilePointer = fopen(fullfile(num2str(subNo), dataFileName), 'a+');
%         fclose(dataFilePointer); 
    else
        dataFilePointer = fopen(fullfile(num2str(subNo), dataFileName), 'a+');
        fprintf(dataFilePointer,'%f, %f, %f, %f, %f, %f, %f, %f\n', ...
            blocCounter, val_async, tactileFirst, ...
            tactileOdd, visOdd, correctRes, subRes, stimulusDuration);
        fclose(dataFilePointer); 
    end
    
   
    
end