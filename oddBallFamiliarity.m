%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Author : Megha Kalia  
% Date : 10th Jan 2022
% Practice to display the difference between odd and non odd stimulus
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% eval(sprintf('mkdir %d',subNum))

%% intiate Serial Port connection 
instrreset 

port = '\\\\.\\COM3';
markStimW('i','\\.\COM3'); 

% open for usage, pause for process delay
pause(2); % test ?

%% variables 
tactileDuration = 70; 
visualDuration = 0; 
visualDelayApplied = 0 ;
tactileDelayApplied = 0; 

% keys for clicks
KbName('UnifyKeyNames'); 
% The avaliable keys to press
escapeKey = KbName('ESCAPE');
% upKey = KbName('UpArrow');
leftKey = KbName('LeftArrow'); % tactile first 
rightKey = KbName('RightArrow'); % visual first 

record_input = false; 

%% start file 
subNum = 1 ; 
subInitials = 'MK'; 
expName = 'oddBallFamiliarity-tactile'; 
b_firstTime = 1; 
write2FileExposure(b_firstTime, expName, subNum); 

correct = 0; 

% intiliaze sound information 
initialization

%% display stimulus 

% Instructions on screen 
Screen('Preference', 'VisualDebugLevel', 1);
Screen('Preference', 'SkipSyncTests', 1);
% screenNumber = max(screens);
[windowPtr,rect] = Screen('OpenWindow', 0, [105,105,105]);
%[windowPtr,rect] = Screen('OpenWindow', 0, [105,105,105],[100 100 1000 480]); % for testing
[ScreenInfo.xaxis, ScreenInfo.yaxis] = Screen('WindowSize',windowPtr);
Screen('TextSize', windowPtr, 60) ;   
Screen('TextFont',windowPtr,'Times');
Screen('TextStyle',windowPtr,1); 

% Show the instructions screen 
DrawFormattedText(windowPtr, ['You will see two types of tactile stimuli. \n\n\n\n' ...
    'Look at your finger and pay attention. \n\n\n\n' ...
    'Press any button to continue. '...
    ], 'center',ScreenInfo.yaxis * 0.25, [0 0 0]); % change color to white 

Screen('Flip', windowPtr);

KbStrokeWait;


% repeat twice 
 repeatition = 1; 
while (repeatition < 3)
    % Display that it is going to be a normal trial
    Screen('TextSize', windowPtr, 60) ;   
    DrawFormattedText(windowPtr, ['Normal Stimulus.\n\n\n Look on your finger'...
        ], 'center',ScreenInfo.yaxis * 0.25, [0 0 0]); % change color to white 
    
    Screen('Flip', windowPtr);
    pause(4);
    
    % Make screen gray 
    DrawFormattedText(windowPtr, [''...
        ], 'center',ScreenInfo.yaxis * 0.25, [0 0 0]); % change color to white 
    Screen('Flip', windowPtr);

    % buzz four times
    temp = 1; 
    tac_bool_odd = 0; 
    vis_bool_odd = 0; 
    while temp < 5 % temp < 9      
        markStimW('w', tactileDuration, 0, tactileDelayApplied, visualDelayApplied, tac_bool_odd, vis_bool_odd);
        pause(1.7); % in seconds      
        temp = temp + 1; 
    end 
    
    % Display that this is oddball trial 
    DrawFormattedText(windowPtr, ['Oddball Stimulus.\n\n\n Look on your finger'...
        ], 'center',ScreenInfo.yaxis * 0.25, [0 0 0]); % change color to white 
    
    Screen('Flip', windowPtr);
    pause(4);
    
    % Make screen gray 
    DrawFormattedText(windowPtr, [''...
        ], 'center',ScreenInfo.yaxis * 0.25, [0 0 0]); % change color to white 
    Screen('Flip', windowPtr);
    
    % buzz odd trial 4 times 
    temp = 1; 
    tac_bool_odd = 1; 
    vis_bool_odd = 0; 
    while temp < 5 % temp < 9      
        markStimW('w', tactileDuration, 0, tactileDelayApplied, visualDelayApplied, tac_bool_odd, vis_bool_odd);
        pause(1.7); % in seconds      
        temp = temp + 1; 
    end 

    repeatition = repeatition + 1;
end

% Screen('TextSize', windowPtr, 60) ;   
DrawFormattedText(windowPtr, ['Which Trial is Odd ? \n\n\n\n' ...
    'First trial (Left Key)         ' ...
    'Second trial (Right Key)'...
    ], 'center',ScreenInfo.yaxis * 0.25, [0 0 0]); % change color to white 

Screen('Flip', windowPtr);
pause(1);

% clear screen
DrawFormattedText(windowPtr, [''], 'center',ScreenInfo.yaxis * 0.25, [0 0 0]); % change color to white 
Screen('Flip', windowPtr);

% first 
% Show the normal trial
% buzz odd trial 4 times 
trialNum = 1; 
tac_bool_odd = 1; 
vis_bool_odd = 0; 
m_sequence = 0 ; 
while trialNum < 10  % 10 times run 
    
    m_sequence = (randperm(2,1)-1); 
    if(m_sequence==0)
        tac_bool_odd = 1; % first oddball 
        markStimW('w', tactileDuration, 0, tactileDelayApplied, visualDelayApplied, tac_bool_odd, vis_bool_odd);
        pause(2.0); % in seconds    
        
        tac_bool_odd = 0; 
        markStimW('w', tactileDuration, 0, tactileDelayApplied, visualDelayApplied, tac_bool_odd, vis_bool_odd);
        pause(2.0); % in seconds   

        correct = 1; 
    else
        tac_bool_odd = 0; 
        markStimW('w', tactileDuration, 0, tactileDelayApplied, visualDelayApplied, tac_bool_odd, vis_bool_odd);
        pause(2.0); % in seconds    
        
        tac_bool_odd = 1;
        markStimW('w', tactileDuration, 0, tactileDelayApplied, visualDelayApplied, tac_bool_odd, vis_bool_odd);
        pause(2.0); % in seconds

        correct = 2; 
    end
    
    % show screen with the question 
    DrawFormattedText(windowPtr, ['Which Trial is Odd ? \n\n\n\n' ...
    'First trial (Left Key)         ' ...
    'Second trial (Right Key)'...
    ], 'center',ScreenInfo.yaxis * 0.25, [0 0 0]); % change color to white 

    Screen('Flip', windowPtr);
    pause(1);
    
    % clear screen
    DrawFormattedText(windowPtr, [''], 'center',ScreenInfo.yaxis * 0.25, [0 0 0]); % change color to white 
    Screen('Flip', windowPtr);

    % get input 
    record_input = true; 
    while record_input == true
    
        % Check the keyboard to see if a button has been pressed
        [keyIsDown,secs, keyCode] = KbCheck;
    
        % Depending on the button press, either move ths position of the square
        % or exit the demo
        if keyCode(escapeKey)
            record_input = false;
        elseif keyCode(leftKey)% correct 1 
            fprintf("leftKey key pressed\n")
            response = 1; 
            record_input = false;
        elseif keyCode(rightKey) % visual % 2
            fprintf("rightKey key pressed\n")
            record_input = false;
            response = 2; 
        end
    end
    
    if(keyCode(leftKey) || keyCode(rightKey) )
        % Log in file 
        b_firstTime = 0; 
        write2FileExposure(b_firstTime, expName, subNum, subInitials, trialNum, 0, 0, ...
            0, 0, correct, response, tactileDuration); 
    end

    % give feedback 
    %% Audio feedback 
   if correct == response % correct 

       % Start audio playback
        PsychPortAudio('Start', pahandle_c, repetitions, startCue, waitForDeviceStart);
        [actualStartTime, ~, ~, estStopTime] = PsychPortAudio('Stop', pahandle_c, 1, 1); 

   else
        % Start audio playback
        PsychPortAudio('Start', pahandle_w, repetitions, startCue, waitForDeviceStart);
        [actualStartTime, ~, ~, estStopTime] = PsychPortAudio('Stop', pahandle_w, 1, 1);
    
        startCue = estStopTime + beepPauseTime;
        
        % Start audio playback
        PsychPortAudio('Start', pahandle_w, repetitions, startCue, waitForDeviceStart);
        PsychPortAudio('Stop', pahandle_w, 1, 1);
   end

   trialNum = trialNum + 1; 
   pause(5);

end 

%% close ports 
markStimW('c',port); 
b_firstTime = 2; % for closing
write2FileExposure(b_firstTime, expName, subNum, subInitials, trialNum, 0, 0, ...
        0, 0, correct, response, tactileDuration); 

PsychPortAudio('Close', pahandle_w);
PsychPortAudio('Close', pahandle_c);










