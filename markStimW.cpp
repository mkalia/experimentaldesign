

#include <Windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
// #include <string>
#include <string.h>
#include <chrono>
#include <thread>
#include <time.h>
#include <fstream>
#include "mex.h"

// Constants for debugging
#define DEBUGGING

// Constants for memory management
// using namespace std;

#define BUFSIZE 1025

//global variables
bool connected_;
HANDLE io_handler_;
COMSTAT status_;
DWORD errors_;
static long *deviceID = NULL;
void openPort(char* com_port, DWORD COM_BAUD_RATE)
{
	connected_ = false;

	io_handler_ = CreateFileA(static_cast<LPCSTR>(com_port),
		GENERIC_READ | GENERIC_WRITE,
		0,
		NULL,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL,
		NULL);

	if (io_handler_ == INVALID_HANDLE_VALUE) {

		if (GetLastError() == ERROR_FILE_NOT_FOUND)
			mexPrintf("Warning: Handle was not attached. Reason: %s not available\n", com_port);
	}
	else {

		DCB dcbSerialParams = { 0 };

		if (!GetCommState(io_handler_, &dcbSerialParams)) {

			mexPrintf("Warning: Failed to get current serial params");
		}

		else {
			dcbSerialParams.BaudRate = COM_BAUD_RATE;
			dcbSerialParams.ByteSize = 8;
			dcbSerialParams.StopBits = ONESTOPBIT;
			dcbSerialParams.Parity = NOPARITY;
			dcbSerialParams.fDtrControl = DTR_CONTROL_ENABLE;

			if (!SetCommState(io_handler_, &dcbSerialParams))
				mexPrintf("Warning: could not set serial port params\n");
			else {
				connected_ = true;
				PurgeComm(io_handler_, PURGE_RXCLEAR | PURGE_TXCLEAR);
			}
		}
	}
}

bool WriteSerialPort(char* data_sent)
{
	DWORD bytes_sent;

	unsigned int data_sent_length = strlen(data_sent);

	if (!WriteFile(io_handler_, (void*)data_sent, data_sent_length, &bytes_sent, NULL)) {
		ClearCommError(io_handler_, &errors_, &status_);
		return false;
	}
	else
		return true;
}

int ReadSerialPortMegha(const char* buffer, unsigned int buf_size)
{
	DWORD bytesRead{};
	unsigned int toRead = 0;

	ClearCommError(io_handler_, &errors_, &status_);

	if (status_.cbInQue > 0)
	{
		if (status_.cbInQue > buf_size)
		{
			toRead = buf_size;
		}
		else
		{
			toRead = status_.cbInQue;
		}
	}

	memset((void*)buffer, 0, buf_size);

	if (ReadFile(io_handler_, (void*)buffer, toRead, &bytesRead, NULL))
	{
		return bytesRead;
	}

	return 0;
}

bool CloseSerialPort()
{
	if (connected_) {
		connected_ = false;
		CloseHandle(io_handler_);
		mexPrintf("port closed \n");
		return true;
	}
	else
    {
        return false;
    }
		
}

void cleanMemory()
{
	if (connected_) {
		connected_ = false;
		CloseHandle(io_handler_);
	}
}

void mexFunction(int nlhs,mxArray *plhs[],int nrhs, const mxArray *prhs[])
{

    char *subCmd, *strParamBuf;
    // size_t strParamBufLen;
    // long baudRate, TTLwindowSize;
    char buf[BUFSIZE] = {'\0'};
    char* output_buf;
    
    size_t size_buff = strlen(buf);
    
    // int nBytesToRead = 0;
    // int bTTL = 0;
    // long TTLPulseWidth = 0;
    // int triggerVal = 0;    // using char crashes. don't know why.
    // int ret = 0;
    
    int megha_triggerVal1 = 0 ;
    int megha_triggerVal2 = 0 ;
    int megha_triggerVal3 = 0 ;
    int megha_triggerVal4 = 0 ;
    int megha_triggerVal5 = 0 ;
    int megha_triggerVal6 = 0 ;
    int megha_triggerVal7 = 0 ;
    
    // check for proper number of arguments
    if (nrhs < 1)
        mexErrMsgTxt("At least one inputs required (string) for the subcommand: '(o)pen', '(r)ead', '(w)rite', or '(c)'lose.");
    
    //Megha added this because I want output for handshake
//    else if (nlhs > 0)
//        mexErrMsgTxt("This mex file does not support output arguments.");
    
    // First input must be a string
    if (mxIsChar(prhs[0]) != 1)
        mexErrMsgTxt("The first input argument must be a string.");
    
//     if (deviceID == NULL)
//     {
//         /* since deviceID is initialized to NULL, we know
//          this is the first call of the MEX-function
//          after it was loaded.  Therefore, we should
//          set up deviceID and the exit function. */
//         /* Allocate array. Use mexMackMemoryPersistent to make the allocated memory persistent in subsequent calls*/
// #ifdef DEBUGGING
//         mexPrintf("First call to MEX-file\n");
// #endif
//         deviceID = mxCalloc(1, 8);    // Why 8, not 4? For double or for long?
//         mexMakeMemoryPersistent(deviceID);
//         mexAtExit(exitFcn);
//     }
    
    // copy the string data from prhs[0] into a C string subCmd.
    subCmd = mxArrayToString(prhs[0]);
    
    if (strcmp(subCmd, "o") == 0 || strcmp(subCmd, "i") == 0)
    {
        // Argument processing
        // if (nrhs < 4)
        //     // TTLwindowSize = 3;    // (ms)
        // else
            // TTLwindowSize = (long)*mxGetPr(prhs[3]);    // copy the numeric data from prhs[3] into a C variable.
        // if (nrhs < 3)
        //     // baudRate = 57600;    // (bit/sec)
        // else
            // baudRate = (long)*mxGetPr(prhs[2]);    // copy the numeric data from prhs[2] into a C variable.
        if (nrhs < 2)
            mexErrMsgTxt("When the first parameter is 'o' or 'i', the second parameter must be a string of the device name path, such as '/dev/cu.usbmodem12341'.");
        else
            strParamBuf = mxArrayToString(prhs[1]);    // copy the string data from prhs[1] into a C string strParamBuf.
        
        // Serial communication
        //deviceID[0] = 999;    //Assign arbitrary number for debugging
        // deviceID[0] = (long) openPort(strParamBuf);    // Open serial port
        

         char com_port[] = "\\\\.\\COM3"; // strParamBuf should be in this format
        // char* com_port= strParamBuf; 
        DWORD COM_BAUD_RATE = CBR_9600; 
        mexPrintf(com_port);
        openPort(strParamBuf, COM_BAUD_RATE);

        if (connected_) {
            mexPrintf("connection established \n");
        }

        std::this_thread::sleep_for(std::chrono::microseconds(3500000)); 
             
        // Free memory
        mxFree(subCmd);
        mxFree(strParamBuf);
    }
    else if (strcmp(subCmd, "t") == 0)    //write a single character !
    {
        char handshake_char[] = { '!' };
        bool is_sent_handshake = WriteSerialPort(handshake_char);
        if (is_sent_handshake < 1)
            mexErrMsgTxt("write() error during handshake"); //temporary suspend
        
        // Free memory
        memset(buf, '\0', BUFSIZE);
        mxFree(subCmd);
    }
    else if (strcmp(subCmd, "z") == 0)    //send four numbers //Megha
    {
        // Argument processing
        if (nrhs < 8)
            mexErrMsgTxt("When the first parameter is 'z', here should be 7 other inputs.");
        else
        megha_triggerVal1 = (int)*mxGetPr(prhs[1]);    // copy the numeric data from prhs[1] into a C variable.
        megha_triggerVal2 = (int)*mxGetPr(prhs[2]);
        megha_triggerVal3 = (int)*mxGetPr(prhs[3]);    // copy the numeric data from prhs[1] into a C variable.
        megha_triggerVal4 = (int)*mxGetPr(prhs[4]);
        megha_triggerVal5 = (int)*mxGetPr(prhs[5]);    // copy the numeric data from prhs[1] into a C variable.
        megha_triggerVal6 = (int)*mxGetPr(prhs[6]);
        megha_triggerVal7 = (int)*mxGetPr(prhs[7]);    // copy the numeric data from prhs[1] into a C variable.
                
        sprintf(buf, "%d,%d,%d,%d,%d,%d,%d", megha_triggerVal1, megha_triggerVal2, megha_triggerVal3, megha_triggerVal4,
                megha_triggerVal5, megha_triggerVal6, megha_triggerVal7); //trying to send data as for parseInt, arduino
         bool is_sent = WriteSerialPort(buf);

         if (is_sent) {
            mexPrintf("send data established \n");
        }
        
#ifdef DEBUGGING
        mexPrintf("%s\n", buf);
#endif      
        //         Free memory
        memset(buf, '\0', BUFSIZE);
        mxFree(subCmd);
    }
    else if (strcmp(subCmd, "y") == 0)    //send four numbers //Megha
    {
        // Argument processing
        if (nrhs < 4)
            mexErrMsgTxt("When the first parameter is 'z', here should be 4 other inputs.");
        else
        megha_triggerVal1 = (int)*mxGetPr(prhs[1]);    // copy the numeric data from prhs[1] into a C variable.
        megha_triggerVal2 = (int)*mxGetPr(prhs[2]);
        megha_triggerVal3 = (int)*mxGetPr(prhs[3]);    // copy the numeric data from prhs[1] into a C variable.
        megha_triggerVal4 = (int)*mxGetPr(prhs[4]);
                
        sprintf(buf, "%d,%d,%d,%d", megha_triggerVal1, megha_triggerVal2, megha_triggerVal3, megha_triggerVal4
                ); //trying to send data as for parseInt, arduino
         bool is_sent = WriteSerialPort(buf);

         if (is_sent) {
            mexPrintf("send data established \n");
        }
        
        //         Free memory
        memset(buf, '\0', BUFSIZE);
        mxFree(subCmd);
    }
    else if (strcmp(subCmd, "w") == 0)    //send 6 numbers //Megha
    {
        // Argument processing
        if (nrhs < 6)
            mexErrMsgTxt("When the first parameter is 'w', here should be 6 other inputs.");
        else
        megha_triggerVal1 = (int)*mxGetPr(prhs[1]);    // copy the numeric data from prhs[1] into a C variable.
        megha_triggerVal2 = (int)*mxGetPr(prhs[2]);
        megha_triggerVal3 = (int)*mxGetPr(prhs[3]);    // copy the numeric data from prhs[1] into a C variable.
        megha_triggerVal4 = (int)*mxGetPr(prhs[4]);
        megha_triggerVal5 = (int)*mxGetPr(prhs[5]);
        megha_triggerVal6 = (int)*mxGetPr(prhs[6]);
                
        sprintf(buf, "%d,%d,%d,%d,%d,%d", megha_triggerVal1, megha_triggerVal2, megha_triggerVal3, 
        megha_triggerVal4, megha_triggerVal5, megha_triggerVal6); //trying to send data as for parseInt, arduino
         bool is_sent = WriteSerialPort(buf);

         if (is_sent) {
            mexPrintf("send data established \n");
        }
        
        //         Free memory
        memset(buf, '\0', BUFSIZE);
        mxFree(subCmd);
    }
    else if(strcmp(subCmd, "m") == 0)
    {
        //read 
        if (ReadSerialPortMegha(buf, strlen("Set") + 2))
        {
            mexPrintf("string is %s\n", buf);
        }
        else
        {
             mexErrMsgTxt("couldn't read string\n.");
        }

        memset(buf, '\0', BUFSIZE);
        mxFree(subCmd);
    }
    else if (strcmp(subCmd, "c") == 0)
    {
        CloseSerialPort();
	    cleanMemory(); 
        
        // Free memory
        mxFree(subCmd);
    }
    else
        mexPrintf("The first parameter can be '(o)pen', '(d)ebug', (r)ead', '(w)rite', or '(c)'lose.\n");
    
    return;
}