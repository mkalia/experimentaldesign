
// pins for LEDs and buzzers
int ledPins[] = {A5, A4, 2, 3, 4, 5, 6, 0};
int buzzerPins[] = {7, 8, 9, 10, 11, 12, 13, 0};

// store ID of chosen buzzer and LED
int ledNo = 0;
int buzzer1No = 0;
int buzzer2No = 0;

// store duration of buzz and flash
int ledDuration;
int buzzDuration;

// store delay of touch
int buzzDelay;

// store delay of vision
int ledDelay;


//store whether transfer is complete
int transferComplete = 0;

unsigned long previousMillis = 0;
unsigned long ledStartMillis = 0;
unsigned long buzzerStartMillis = 0;

// the setup routine runs once when you press reset:
void setup() {
  for (int i = 0; i <= 7; i++) {
    pinMode(ledPins[i], OUTPUT);
    pinMode(buzzerPins[i], OUTPUT);
  }
  //open serial port
  Serial.begin(9600);
}

void loop() {
  if (Serial.available() > 0) {
    //make sure we have all the data
    delay(10);

    // Read the incoming comma separated integers
    buzzer1No = Serial.parseInt() - 1;
    buzzer2No = Serial.parseInt() - 1;
    ledNo = Serial.parseInt() - 1;
    buzzDuration = Serial.parseInt();
    ledDuration = Serial.parseInt();
    buzzDelay = Serial.parseInt();
    ledDelay = Serial.parseInt();

    //read out start time
    previousMillis = millis();
    // reset start timers
    ledStartMillis = 0;
    buzzerStartMillis = 0;

  }

  // store current time
  unsigned long currentMillis = millis();
  //wait for given duration
  if (currentMillis - previousMillis >= ledDelay) {
    // reset start timer only once
    if (ledStartMillis == 0) {
      // store start of the visual stimulus
      ledStartMillis = millis();
    }
    if (currentMillis - ledStartMillis <= ledDuration) {
      // switch the led on
      digitalWrite(ledPins[ledNo], 1);
    }
    else {
      //switch LED off
      digitalWrite(ledPins[ledNo], 0);
    }
  }
  if (currentMillis - previousMillis >= buzzDelay) {
    // reset start timer only once
    if (buzzerStartMillis == 0) {
      // store start of the visual stimulus
      buzzerStartMillis = millis();
    }
    if (currentMillis - buzzerStartMillis <= buzzDuration) {
      // switch buzzer on
      digitalWrite(buzzerPins[buzzer1No], 1);
      digitalWrite(buzzerPins[buzzer2No], 1);
    }
    else {
      // switch buzzer off
      digitalWrite(buzzerPins[buzzer1No], 0);
      digitalWrite(buzzerPins[buzzer2No], 0);
    }
  }
}






