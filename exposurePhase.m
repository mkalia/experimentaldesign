%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Author : Megha Kalia 
% Date : 10th Jan 2022
% Display asynchrony of 150ms each for 8 times staight. 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


clear all; 

KbName('UnifyKeyNames');

%% subject information 
subNum = 1 ; 
subInitials = 'MK'; 

%% intiate Serial Port connection 
instrreset 

port = '\\\\.\\COM3';
markStimW('i','\\.\COM3'); 

% open for usage, pause for process delay
pause(2); % test ?

%% start file 
subNum = 1 ; 
subInitials = 'MK'; 
expName = 'ExposureFirst'; 
b_firstTime = 1; 
write2FileExposure(b_firstTime, expName, subNum); 

correct = 0; 

%% set up screen 
% Instructions on screen 
Screen('Preference', 'VisualDebugLevel', 1);
Screen('Preference', 'SkipSyncTests', 1);
% screenNumber = max(screens);
[windowPtr,rect] = Screen('OpenWindow', 0, [105,105,105]);
%[windowPtr,rect] = Screen('OpenWindow', 0, [105,105,105],[100 100 1000 480]); % for testing
[ScreenInfo.xaxis, ScreenInfo.yaxis] = Screen('WindowSize',windowPtr);
Screen('TextSize', windowPtr, 45) ;   
Screen('TextFont',windowPtr,'Times');
Screen('TextStyle',windowPtr,1); 

% Show the instructions screen 
DrawFormattedText(windowPtr, ['Select the odd trial.\n\n\n' ...
    'None\n\n\n'
    ' visual      tactile \n\n\n'...
    'Both'
    ], 'center',ScreenInfo.yaxis * 0.25, [0 0 0]); % change color to white 

DrawFormattedText(windowPtr, ['Click any button to continue\n'
    ], 'center',ScreenInfo.yaxis * 0.25, [0 0 0]); % change color to white 

Screen('Flip', windowPtr);

KbStrokeWait;



%% set variables (units ms)
tactileDuration = 70; 
visualDuration = 70; 
visualDelayApplied = 0 ;
tactileDelayApplied = 0; 

if mod(subNum,2) == 0  % for even subjects to balance out order effects
    tactileDelayApplied = 150; % equivalent to -150, vision first 
else
    visualDelayApplied = 150; 
end

% vectorTactileDelay  = zeros(1,8) + visualDelayApplied; % -1 condition
% vectorVisualDelay   = zeros(1,8) + tactileDelayApplied;  

%% Stimulus 
% The avaliable keys to press
escapeKey = KbName('ESCAPE');
% upKey = KbName('UpArrow');
downKey = KbName('DownArrow'); % simultaneous
leftKey = KbName('LeftArrow'); % tactile first 
rightKey = KbName('RightArrow'); % visual first 

% 
% idx of trials to corrupt 
num_odd_tactile = randi(3,1,1); % 0-3 odd trials 
num_odd_visual  = randi(3,1,1); % 0-3 odd trials

idx_odd_tactile = randperm(8,num_odd_tactile) % range 1-8 number num_odd_tactile
idx_odd_visual  = randperm(8,num_odd_visual); 

tac_bool_odd = 0; 
vis_bool_odd = 0; 

% wrong 
% vectorTactileDelay(idx_odd_tactile) = 1; % set this value 
% vectorVisualDelay(idx_odd_visual)  = 1; 

% repeat 30 times % TODO
temp = 1 ; % loop runs for temp 1-8
while temp < 9 

    if any(idx_odd_tactile(:) == temp)
        tac_bool_odd = 1; 
    end

    if any(idx_odd_visual(:) == temp)
        vis_bool_odd = 1; 
    end
    
%     if any(idx_odd_tactile(:) == temp)
%         tac_bool_odd = 0; 
%         markStimW('w', tactileDuration+20, visualDuration, tactileDelayApplied, visualDelayApplied, tac_bool_odd, vis_bool_odd);
%     end
% 
%     if any(idx_odd_visual(:) == temp)
%         vis_bool_odd = 0; 
%         markStimW('w', tactileDuration, visualDuration+20, tactileDelayApplied, visualDelayApplied, tac_bool_odd, vis_bool_odd);
%     end
     markStimW('w', tactileDuration, visualDuration, tactileDelayApplied, visualDelayApplied, tac_bool_odd, vis_bool_odd);
    % 
    vis_bool_odd = 0; 
    tac_bool_odd = 0;

    pause(1.7); % in seconds 
  
    temp = temp + 1; 
end 

% get input and log
    record_input = true; 
    while record_input == true
    
        % Check the keyboard to see if a button has been pressed
        [keyIsDown,secs, keyCode] = KbCheck;
    
        % Depending on the button press, either move ths position of the square
        % or exit the demo
        if keyCode(escapeKey)
            record_input = false;
        elseif keyCode(leftKey)% tactile
            fprintf("leftKey key pressed\n")
            response = 1; 
            record_input = false;
        elseif keyCode(rightKey) % visual
            fprintf("rightKey key pressed\n")
            record_input = false;
            response = 2; 
        elseif keyCode(downKey) % simultaneous
            fprintf("downKey key pressed\n")
            record_input = false;
        end
    end
    
    if(keyCode(leftKey) || keyCode(rightKey) || keyCode(downKey) )
        % Log in file 
        b_firstTime = 0; 
        write2FileExposure(b_firstTime, expName, subNum, subInitials, blocCounter, tactileDelayApplied, (tactileDelayApplied>0), ...
            num_odd_tactile, num_odd_visual, correct, response, tactileDuration); 
    end

pause(5)

%% clean variables
markStimW('c',port); 
b_firstTime = 2; % for closing
write2FileExposure(b_firstTime, expName, subNum, subInitials, blocCounter, tactileDelayApplied, (tactileDelayApplied>0), ...
        num_odd_tactile, num_odd_visual, correct, response, tactileDuration); 

markStimW('c',port); 



