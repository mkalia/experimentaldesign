
// pins for LEDs and buzzers
// Author : Megha Kalia
// 10th Jan 2022

//int ledPins[] = {A5, A4, 2, 3, 4, 5, 6, 0};
//int buzzerPins[] = {7, 8, 9, 10, 11, 12, 13, 0};

int ledPins[] = {A3, A5, A4, 2, 3, 4, A2, 6, 5};
int buzzerPins[] = {A1, 7, 8, 9, 10, A0, 12, 13, 11};

// store ID of chosen buzzer and LED
int ledNo = 0;
int buzzer1No = 0;
int buzzer2No = 0;

// store duration of buzz and flash
int ledDuration;
int buzzDuration;

// store delay of touch
int buzzDelay;

// store delay of vision
int ledDelay;

//store whether transfer is complete
int transferComplete = 0;

unsigned long previousMillis = 0;
unsigned long ledStartMillis = 0;
unsigned long buzzerStartMillis = 0;

int tac_bool_odd = 0 ; 
int vis_bool_odd = 0 ; 

// the setup routine runs once when you press reset:
void setup() {
  for (int i = 0; i <= 8; i++) {
    pinMode(ledPins[i], OUTPUT);
    pinMode(buzzerPins[i], OUTPUT);
  }
  //open serial port
  Serial.begin(9600); //test with different baud rates 
}

void loop() {
  if (Serial.available() > 0) {
    //make sure we have all the data
    delay(10);

    // Read the incoming comma separated integers
    //buzzer1No = Serial.parseInt() - 1;
//    buzzer2No = Serial.parseInt() - 1;
//    ledNo = Serial.parseInt() - 1;
    buzzDuration = Serial.parseInt();
    ledDuration = Serial.parseInt();
    buzzDelay = Serial.parseInt();
    ledDelay = Serial.parseInt();
    tac_bool_odd = Serial.parseInt();
    vis_bool_odd = Serial.parseInt();

//    buzzDuration = 1000; 
//    ledDuration = 1000; 
    
    
    //read out start time
    previousMillis = millis();
    // reset start timers
    ledStartMillis = 0;
    buzzerStartMillis = 0;

  }

  // store current time
  unsigned long currentMillis = millis();
  //wait for given duration
  if (currentMillis - previousMillis >= ledDelay) {
    // reset start timer only once
    if (ledStartMillis == 0) {
      // store start of the visual stimulus
//      ledStartMillis = millis(); //this ideally should be very close to currentMillis intially, thus difference is close to 0 
      ledStartMillis = millis(); //This is to set th start to zero
      
    }
    
    if ((currentMillis - ledStartMillis) <= ledDuration)  {
      // switch the led on

      if(vis_bool_odd == 1)
      {
//        digitalWrite(ledPins[8], 1);
        analogWrite(ledPins[8], 30);
      }
      else
      {
        analogWrite(ledPins[8], 2);
      }
      
    }
    else {
      //switch LED off

      if(vis_bool_odd==1)
      {
//        digitalWrite(ledPins[8], 0);
        analogWrite(ledPins[8], 0);
      }
      else
      {
        analogWrite(ledPins[8], 0);
      }
      
    }
  }
  if (currentMillis - previousMillis >= buzzDelay) {
    // reset start timer only once
    if (buzzerStartMillis == 0) {
      // store start of the visual stimulus
      buzzerStartMillis = millis();
//      buzzerStartMillis = millis();
    }
    if ((currentMillis - buzzerStartMillis) <= buzzDuration) {
      // switch buzzer on
      if(tac_bool_odd == 1)
      {
//        analogWrite(buzzerPins[8], 170);
        
//        digitalWrite(buzzerPins[7], 1);
        analogWrite(buzzerPins[8], 180);
      }
      else
      {
        digitalWrite(buzzerPins[8], 1);
//        analogWrite(buzzerPins[7], 200);

//        digitalWrite(buzzerPins[8], 1);
//        digitalWrite(buzzerPins[7], 1);
      }
      
 
    }
    else {
      // switch buzzer off
      if(tac_bool_odd == 1)
      {
        digitalWrite(buzzerPins[8], 0);
//        digitalWrite(buzzerPins[7], 0);
      }
      else
      {
        digitalWrite(buzzerPins[8], 0);
        analogWrite(buzzerPins[8], 0);
//        analogWrite(buzzerPins[7], 0);
//        digitalWrite(buzzerPins[8], 0);
//        digitalWrite(buzzerPins[7], 0);
      }
    }
  }
}
