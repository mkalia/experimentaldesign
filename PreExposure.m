
%% initialization 

for i = 1:11
    eval(sprintf('cond_num_%d = 0 ', i))
end

bool_draw_rand = 1; 
num_repetitions = 20; 
idx_cond = 0 ; 

tactileDuration = 50; 
visualDuration = 50; 
visualDelayApplied = 0 ;
tactileDelayApplied = 0; 

%% implemention 

cond_list = [-240, -120, -90, -60, -30, 0, 30, 60, 90, 120, 240]; % condition list 

while (bool_draw_rand)

    idx_cond = randi(len(cond_list),1,1); % select a condition randomly 
    
    % check if a condition has reached max repetitions 
    if(eval(sprintf('cond_num_%d >= num_repetitions', idx_cond)));        
        cond_list(idx_cond) = []; % pop the condition 
        bool_draw_rand = 1 ; % repeat the loop again 
    else
        bool_draw_rand = 0; % don't repreat this loop and move forward
        
        %BEWARE: increament should be after recording the userinput
        eval(sprintf('cond_num_%d = cond_num_%d + 1', idx_cond));
    end
end

if (cond_list(idx_cond) < 0) % visual first or tactile delay
    tactileDelayApplied = cond_list(idx_cond) ; 
    visualDelayApplied = 0 ; 
else
    visualDelayApplied = cond_list(idx_cond) ; % tactile first, visual delayed
    tactileDelayApplied = 0 ; 
end

markStimW('y',tactileDuration, visualDuration, tactileDelayApplied, visualDelayApplied);
pause(2.5); % in seconds 

bool_draw_rand = 1; % re-run the loop for next condition

%% User input and log it









