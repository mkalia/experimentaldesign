

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Author : Megha Kalia  
% Date : 10th Jan 2022
% Practice Exposure and VT ordinary judgement
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all; 

% intiliaze sound information 
initialization

% keys for clicks
KbName('UnifyKeyNames'); 

%% subject information 

subNum = 1 ; 
subInitials = 'MK'; 
expName = 'PracticeExposure'; 
b_firstTime = 1; 

eval(sprintf('mkdir %d',subNum))

%% intiate Serial Port connection 
instrreset 

port = '\\\\.\\COM3';
markStimW('i','\\.\COM3'); 

% open for usage, pause for process delay
pause(2); % test ?

%% set up screen 
% set up visual stimulus
% addpath causing probem
% addpath(genpath('C:\Users\megha\source\MATLABToolBoxes\Psychtoolbox')); %


Screen('Preference', 'VisualDebugLevel', 1);
Screen('Preference', 'SkipSyncTests', 1);
% screenNumber = max(screens);
[windowPtr,rect] = Screen('OpenWindow', 0, [105,105,105]);
%[windowPtr,rect] = Screen('OpenWindow', 0, [105,105,105],[100 100 1000 480]); % for testing
[ScreenInfo.xaxis, ScreenInfo.yaxis] = Screen('WindowSize',windowPtr);
Screen('TextSize', windowPtr, 45) ;   
Screen('TextFont',windowPtr,'Times');
Screen('TextStyle',windowPtr,1); 

% Show the instructions screen 
DrawFormattedText(windowPtr, ['Which has greater number of odd trials \n \n\n' ...
    '\n\nTactile (Left Arrow), Simultaneous (Down Arrow), Vision (Right Arrow)' ...
    '\n\n\n\n\n\n\n Press any key to continue'...
    ], 'center',ScreenInfo.yaxis * 0.25, [0 0 0]); % change color to white 

Screen('Flip', windowPtr);

% Now we have drawn to the screen we wait for a keyboard button press (any
% key) to terminate the demo
KbStrokeWait;

% Clear the screen
sca;

% start file 
write2FileExposure(b_firstTime, expName, subNum); 

%% set variables (units ms)
tactileDuration = 70; 
visualDuration = 70; 
visualDelayApplied = 0 ;
tactileDelayApplied = 0; 

% in practice trials asynchorny is 0 
if mod(subNum,2) == 0  % for even subjects to balance out order effects
    tactileDelayApplied = 0; % equivalent to -150, vision first 
else
    visualDelayApplied = 0; 
end

% vectorTactileDelay  = zeros(1,8) + visualDelayApplied; % -1 condition
% vectorVisualDelay   = zeros(1,8) + tactileDelayApplied;  

%% Stimulus 
% The avaliable keys to press
escapeKey = KbName('ESCAPE');
% upKey = KbName('UpArrow');
downKey = KbName('DownArrow'); % simultaneous
leftKey = KbName('LeftArrow'); % tactile first 
rightKey = KbName('RightArrow'); % visual first 

response = 0;  % 1 for tactile and 2 for visual , 0 is for simultaneous 
correct = 0; 
% 
numBlocks = 3; 
for blocCounter = 1:numBlocks

    % idx of trials to corrupt 
    num_odd_tactile = randi(3,1,1); % 0-3 odd trials 
    num_odd_visual  = randi(3,1,1) % 0-3 odd trials
    
    % enforce tactile and visual to be of different lengths 
    if (num_odd_tactile == num_odd_visual)    
        % randomly increase either visual  or tactile
        if((randperm(2,1)-1)==0)
            num_odd_tactile = num_odd_tactile + 1 
        else
            num_odd_visual = num_odd_visual + 1 
        end       
    else
        num_odd_tactile % only for debugging
    end
    
    idx_odd_tactile = randperm(8,num_odd_tactile) ;% range 1-8 number num_odd_tactile
    idx_odd_visual  = randperm(8,num_odd_visual); 

    if num_odd_tactile > num_odd_visual  
        correct = 1; 
    else
        correct = 2; % vision is greater
    end

    tac_bool_odd = 0; 
    vis_bool_odd = 0; 
    % numBlocks = 10; % repeat 1 times

    temp = 1 ; % loop runs for temp 1-8
    while temp < 9 % temp < 9 
    
        if any(idx_odd_tactile(:) == temp)
            tac_bool_odd = 1; 
        end
    
        if any(idx_odd_visual(:) == temp)
            vis_bool_odd = 1; 
        end
       
        markStimW('w', tactileDuration, visualDuration, tactileDelayApplied, visualDelayApplied, tac_bool_odd, vis_bool_odd);
        % 
        vis_bool_odd = 0; 
        tac_bool_odd = 0;
    
        pause(1.7); % in seconds 
      
        temp = temp + 1; 
    end 
    
    % get input 
    record_input = true; 
    while record_input == true
    
        % Check the keyboard to see if a button has been pressed
        [keyIsDown,secs, keyCode] = KbCheck;
    
        % Depending on the button press, either move ths position of the square
        % or exit the demo
        if keyCode(escapeKey)
            record_input = false;
        elseif keyCode(leftKey)% tactile
            fprintf("leftKey key pressed\n")
            response = 1; 
            record_input = false;
        elseif keyCode(rightKey) % visual
            fprintf("rightKey key pressed\n")
            record_input = false;
            response = 2; 
        elseif keyCode(downKey) % simultaneous
            fprintf("downKey key pressed\n")
            record_input = false;
        end
    end
    
    if(keyCode(leftKey) || keyCode(rightKey) || keyCode(downKey) )
        % Log in file 
        b_firstTime = 0; 
        write2FileExposure(b_firstTime, expName, subNum, subInitials, blocCounter, tactileDelayApplied, (tactileDelayApplied>0), ...
            num_odd_tactile, num_odd_visual, correct, response, tactileDuration); 
    end
    
    
    %% Audio feedback 
   if correct == response % correct 

       % Start audio playback
        PsychPortAudio('Start', pahandle_c, repetitions, startCue, waitForDeviceStart);
        [actualStartTime, ~, ~, estStopTime] = PsychPortAudio('Stop', pahandle_c, 1, 1); 

   else
        % Start audio playback
        PsychPortAudio('Start', pahandle_w, repetitions, startCue, waitForDeviceStart);
        [actualStartTime, ~, ~, estStopTime] = PsychPortAudio('Stop', pahandle_w, 1, 1);
    
        startCue = estStopTime + beepPauseTime;
        
        % Start audio playback
        PsychPortAudio('Start', pahandle_w, repetitions, startCue, waitForDeviceStart);
        PsychPortAudio('Stop', pahandle_w, 1, 1);
   end

    pause(5); 
end


%% clean variables
markStimW('c',port); 
b_firstTime = 2; % for closing
write2FileExposure(b_firstTime, expName, subNum, subInitials, blocCounter, tactileDelayApplied, (tactileDelayApplied>0), ...
        num_odd_tactile, num_odd_visual, correct, response, tactileDuration); 

PsychPortAudio('Close', pahandle_w);
PsychPortAudio('Close', pahandle_c);


%% TOJ task





